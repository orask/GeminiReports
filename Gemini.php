<?php
include "httpful.phar";
include "TicketStatus.php";
include "GeminiTicket.php";
include "GeminiTicketTimeEntry.php";

class Gemini
{
    private $url;
    private $headers;

    public function __construct($url, $headers)
    {
        $this->url = $url;
        //$this->authentication = $authentication;
        $this->headers = $headers;
    }

    public function getListOfStatuses()
    {
        $uri = $this->url . '/api/status';
        $result = \Httpful\Request::get($uri)->addHeaders($this->headers)->send();
        //echo "===============\n\n\n\n" .
        print_r($result->body); // "===============\n\n\n\n";
    }

    public function getListOfTicketsPerProjectPerStatus($project, $status, $sprint)
    {
        $uri = $this->url . '/api/items/filtered';
        $body = array(
            "Projects"         => "{$project}",
            "Statuses"         => "{$status}",
            "Versions"         => "{$sprint}",
            "MaxItemsToReturn" => 50
        );
        $result = \Httpful\Request::post($uri)->addHeaders($this->headers)->body(json_encode($body))->send();

        $tickets = array();
        foreach ($result->body as $key => $value) {
            $ticket = new GeminiTicket();
            $ticket->setId($value->Id);
            $ticket->setTitle($value->Title);
            $ticket->setType($value->Type);
            $ticket->setStatus($value->Status);
            $ticket->setPoints($value->Points);
            $ticket->setComponentNames($value->ComponentNames);
            $ticket->setEstimatedHours($value->EstimatedHours);
            $ticket->setEstimatedMinutes($value->EstimatedMinutes);
            $ticket->setLoggedHours($value->LoggedHours);
            $ticket->setLoggedMinutes($value->LoggedMinutes);
            $ticket->setPercentComplete($value->PercentComplete);
//            echo "[".count($value->TimeEntries)."]\n";
//            if(count($value->TimeEntries) > 0) {
//                print_r($value->TimeEntries); exit;
//            }
            if (is_array($value->TimeEntries) && count($value->TimeEntries) > 0) {
                foreach ($value->TimeEntries as $key2 => $entry) {
                    $timeEntry = new GeminiTicketTimeEntry();
                    $timeEntry->setFullName($entry->Fullname);
                    $timeEntry->setTimeType($entry->TimeType);
                    $timeEntry->setEntryDate($entry->BaseEntity->EntryDate);
                    $timeEntry->setMinutes($entry->BaseEntity->Minutes);
                    $timeEntry->setHours($entry->BaseEntity->Hours);
                    $ticket->setTimeEntries($timeEntry);
                }
            }

            $tickets[] = $ticket;
        }

        return $tickets;

    }

    public function getVersionsOfProject()
    {
        $uri = $this->url . '/api/projects/22/versions';
        $result = \Httpful\Request::get($uri)->addHeaders($this->headers)->send();

        return json_encode($result->body);
    }

    public function getReportPerColumn($project, $status, $sprint)
    {
        $tickets = $this->getListOfTicketsPerProjectPerStatus($project, $status, $sprint);
        $totalTickets = 0;
        $totalLoggedTime = 0;
        $totalEstimatedTime = 0;
        $totalPoints = 0;
        $statuses = array();
        $types = array();

        $timeRecords = array();

        /**
         * @var GeminiTicket $ticket
         */
        foreach ($tickets as $key => $ticket) {
            $totalTickets++;
            $totalPoints += $ticket->getPoints();
            $totalEstimatedTime += $ticket->getEstimatedHours() + $ticket->getEstimatedMinutes() / 60;
            $totalLoggedTime += $ticket->getLoggedHours() + $ticket->getLoggedMinutes() / 60;

            if (isset($statuses[ $ticket->getStatus() ])) {
                $statuses[ $ticket->getStatus() ]++;
            } else {
                $statuses[ $ticket->getStatus() ] = 1;
            }
            if (isset($types[ $ticket->getType() ])) {
                $types[ $ticket->getType() ]++;
            } else {
                $types[ $ticket->getType() ] = 1;
            }


            if ($ticket->getTimeEntries()) {
                $records = $ticket->getTimeEntries();
                foreach ($records as $key3 => $record) {
                    $t = strtotime($record->getEntryDate());
                    $byDate = date('d/m/Y', $t);
                    if (!isset($timeRecords[ $byDate ]['hours'])) {
                        $timeRecords[ $byDate ]['hours'] = 0;
                    }
                    if (!isset($timeRecords[ $byDate ]['minutes'])) {
                        $timeRecords[ $byDate ]['minutes'] = 0;
                    }


                    if (!isset($timeRecords[ $record->getTimeType() ]['hours'])) {
                        $timeRecords[ $record->getTimeType() ]['hours'] = 0;
                    }
                    if (!isset($timeRecords[ $record->getTimeType() ]['minutes'])) {
                        $timeRecords[ $record->getTimeType() ]['minutes'] = 0;
                    }
                    $timeRecords[ $record->getTimeType() ]['hours'] += $record->getHours();
                    $timeRecords[ $record->getTimeType() ]['minutes'] += $record->getMinutes();
                    $this->refineHoursMinutes($timeRecords[ $record->getTimeType() ]['hours'], $timeRecords[ $record->getTimeType() ]['minutes']);


                    if (!isset($timeRecords[ $record->getFullName() ]['minutes'])) {
                        $timeRecords[ $record->getFullName() ]['minutes'] = 0;
                    }
                    if (!isset($timeRecords[ $record->getFullName() ]['hours'])) {
                        $timeRecords[ $record->getFullName() ]['hours'] = 0;
                    }
                    $timeRecords[ $record->getFullName() ]['hours'] += $record->getHours();
                    $timeRecords[ $record->getFullName() ]['minutes'] += $record->getMinutes();
                    $this->refineHoursMinutes($timeRecords[ $record->getFullName() ]['hours'], $timeRecords[ $record->getFullName() ]['minutes']);

                    $timeRecords[ $byDate ]['hours'] += $record->getHours();
                    $timeRecords[ $byDate ]['minutes'] += $record->getMinutes();

                    $this->refineHoursMinutes($timeRecords[ $byDate ]['hours'], $timeRecords[ $byDate ]['minutes']);
                }
            }
        }
        echo "Total Tickets = " . $totalTickets . "\n\n";
        echo "Total Estimated Hours = " . $totalEstimatedTime . "\n\n";
        echo "Total Logged Time = " . $totalLoggedTime . "\n\n";
        echo "Total Points = " . $totalPoints . "\n\n";
        echo "Total Items = " . json_encode($statuses) . "\n\n";
        echo "Total Types = " . json_encode($types) . "\n\n";
        echo "===============\n\n";
        echo json_encode($timeRecords);
    }

    public function refineHoursMinutes(&$hours, &$minutes)
    {
        $addedHours = (int)($minutes / 60);
        $hours += $addedHours;
        $minutes = $minutes % 60;

//        return array('hours'   => $hours + $addedHours,
//                     'minutes' => $minutes
//        );

    }
}