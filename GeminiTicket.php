<?php

class GeminiTicket
{
    protected $id;
    protected $title;
    protected $status;
    protected $type;
    protected $componentNames;
    protected $estimatedHours;
    protected $estimatedMinutes;
    protected $loggedHours;
    protected $loggedMinutes;
    protected $points;
    protected $loggedIncludingKids;
    protected $percentComplete;
    /**
     * @var Array<GeminiTicketTimeEntry>
     */
    protected $timeEntries;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getComponentNames()
    {
        return $this->componentNames;
    }

    /**
     * @param mixed $componentNames
     */
    public function setComponentNames($componentNames)
    {
        $this->componentNames = $componentNames;
    }

    /**
     * @return mixed
     */
    public function getEstimatedHours()
    {
        return $this->estimatedHours;
    }

    /**
     * @param mixed $estimatedHours
     */
    public function setEstimatedHours($estimatedHours)
    {
        $this->estimatedHours = $estimatedHours;
    }

    /**
     * @return mixed
     */
    public function getEstimatedMinutes()
    {
        return $this->estimatedMinutes;
    }

    /**
     * @param mixed $estimatedMinutes
     */
    public function setEstimatedMinutes($estimatedMinutes)
    {
        $this->estimatedMinutes = $estimatedMinutes;
    }

    /**
     * @return mixed
     */
    public function getLoggedHours()
    {
        return $this->loggedHours;
    }

    /**
     * @param mixed $loggedHours
     */
    public function setLoggedHours($loggedHours)
    {
        $this->loggedHours = $loggedHours;
    }

    /**
     * @return mixed
     */
    public function getLoggedMinutes()
    {
        return $this->loggedMinutes;
    }

    /**
     * @param mixed $loggedMinutes
     */
    public function setLoggedMinutes($loggedMinutes)
    {
        $this->loggedMinutes = $loggedMinutes;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @param mixed $points
     */
    public function setPoints($points)
    {
        $this->points = $points;
    }

    /**
     * @return mixed
     */
    public function getLoggedIncludingKids()
    {
        return $this->loggedIncludingKids;
    }

    /**
     * @param mixed $loggedIncludingKids
     */
    public function setLoggedIncludingKids($loggedIncludingKids)
    {
        $this->loggedIncludingKids = $loggedIncludingKids;
    }

    /**
     * @return mixed
     */
    public function getPercentComplete()
    {
        return $this->percentComplete;
    }

    /**
     * @param mixed $percentComplete
     */
    public function setPercentComplete($percentComplete)
    {
        $this->percentComplete = $percentComplete;
    }

    /**
     * @return Array
     */
    public function getTimeEntries()
    {
        return $this->timeEntries;
    }

    /**
     * @param Array $timeEntries
     */
    public function setTimeEntries($timeEntries)
    {
        $this->timeEntries[] = $timeEntries;
    }

}