<?php

Class TicketStatus
{
    const SPRINT_BACKLOG = 171;
    const DEVELOPMENT = 173;
    const CODE_REVIEW = 174;
    const QA_BACKLOG = 227;
    const QA_IN_PROGRESS = 267;
    const UAT = 268;
    const AWAITING_RELEASE = 177;
    const RELEASED = 178;
}