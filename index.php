<?php
//use Httpful;

include "config.php";
include "Gemini.php";
//include "TicketStatus.php";
include "TicketSprint.php";

$myClass = new Gemini($url, $headers);

//$myClass->getListOfStatuses();
//$myClass->getListOfTicketsPerProjectPerStatus();
//$myClass->getVersionsOfProject();

$allTickets = sprintf("%s|%s|%s|%s|%s|%s|%s|%s",
    TicketStatus::SPRINT_BACKLOG,
    TicketStatus::DEVELOPMENT,
    TicketStatus::CODE_REVIEW,
    TicketStatus::QA_BACKLOG,
    TicketStatus::QA_IN_PROGRESS,
    TicketStatus::UAT,
    TicketStatus::AWAITING_RELEASE,
    TicketStatus::RELEASED
);

$myClass->getReportPerColumn(22, $allTickets, TicketSprint::SPRINT9);

echo "\n\n\n\n";